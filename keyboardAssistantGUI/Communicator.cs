﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Windows;

namespace keyboardAssistantGUI
{
    public struct Response
    {
        public int CorrectionOrContinuation;
        public string word1;
        public string word2;
        public string word3;
    }
    public struct Request
    {
        public int CorrectionOrContinuation;
        public string word;
    }
    static public class Communicator
    {
        const string ip = "127.0.0.1";
        const int port = 4532;
        static NetworkStream _sock;
        static JsonSerializer _serializer = new JsonSerializer();
        private static void Send(byte[] buffer, int offset, int length)
        {
            _sock.Write(buffer, offset, length);
        }
        private static void Read(byte[] buffer, int offset, int length)
        {
            try
            {
                _sock.Read(buffer, offset, length);
            }
            catch(Exception e)
            {
                buffer = null;
            }
        }
        public static void SendWord(int correctionOrContinuation, string word)
        {
            Request request = new Request();
            request.CorrectionOrContinuation = correctionOrContinuation;
            request.word = word;
            SendRequest(0, request);
        }
        private static void SendRequest(int code, object request)
        {
            string serializedRequest = JsonConvert.SerializeObject(request);
            byte[] messageBytes = Encoding.ASCII.GetBytes(serializedRequest);
            byte[] buffer = new byte[messageBytes.Length + 5];
            buffer[0] = (byte)code;
            //copying message length to bytes 1-4
            Array.Copy(BitConverter.GetBytes(messageBytes.Length), 0, buffer, 1, sizeof(int));
            //copying message to the rest of buffer
            Array.Copy(messageBytes, 0, buffer, 5, messageBytes.Length);
            Send(buffer, 0, buffer.Length);
        }
        private static byte[] Recv()
        {
            byte[] codeAndLen = new byte[5];
            Read(codeAndLen, 0, 5);
            int len = BitConverter.ToInt32(codeAndLen, 1);
            //represents the entire message
            byte[] buffer = new byte[len + 5];
            Array.Copy(codeAndLen, 0, buffer, 0, 5);
            Read(buffer, 5, len);
            return buffer;
        }
        public static Response recvResponse()
        {
            byte[] recieved = Recv();
            string data = Encoding.ASCII.GetString(recieved, 5, recieved.Length - 5);
            Response response = new Response();
            try
            {
                response = JsonConvert.DeserializeObject<Response>(data);
            }
            catch
            {
                return response;
            }
            return response;
        }
        public static void Connect()
        {
            TcpClient tcpClient = new TcpClient();
            IPAddress address = IPAddress.Parse(ip);
            IPEndPoint remoteEP = new IPEndPoint(address, port);
            tcpClient.Connect(remoteEP);
            _sock = tcpClient.GetStream();
        }
    }
}
