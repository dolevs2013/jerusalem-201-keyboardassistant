﻿namespace keyboardAssistantGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Corr1 = new System.Windows.Forms.Button();
            this.correctionLBL = new System.Windows.Forms.Label();
            this.continuationsLBL = new System.Windows.Forms.Label();
            this.Corr2 = new System.Windows.Forms.Button();
            this.Corr3 = new System.Windows.Forms.Button();
            this.Cont3 = new System.Windows.Forms.Button();
            this.Cont2 = new System.Windows.Forms.Button();
            this.Cont1 = new System.Windows.Forms.Button();
            this.ExitBTN = new System.Windows.Forms.Button();
            this.updateBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Corr1
            // 
            this.Corr1.Location = new System.Drawing.Point(16, 56);
            this.Corr1.Name = "Corr1";
            this.Corr1.Size = new System.Drawing.Size(105, 63);
            this.Corr1.TabIndex = 0;
            this.Corr1.UseVisualStyleBackColor = true;
            this.Corr1.Click += new System.EventHandler(this.Corr1_Click);
            // 
            // correctionLBL
            // 
            this.correctionLBL.AutoSize = true;
            this.correctionLBL.Location = new System.Drawing.Point(13, 13);
            this.correctionLBL.Name = "correctionLBL";
            this.correctionLBL.Size = new System.Drawing.Size(63, 13);
            this.correctionLBL.TabIndex = 1;
            this.correctionLBL.Text = "Corrections:";
            // 
            // continuationsLBL
            // 
            this.continuationsLBL.AutoSize = true;
            this.continuationsLBL.Location = new System.Drawing.Point(518, 13);
            this.continuationsLBL.Name = "continuationsLBL";
            this.continuationsLBL.Size = new System.Drawing.Size(74, 13);
            this.continuationsLBL.TabIndex = 2;
            this.continuationsLBL.Text = "Continuations:";
            // 
            // Corr2
            // 
            this.Corr2.Location = new System.Drawing.Point(163, 56);
            this.Corr2.Name = "Corr2";
            this.Corr2.Size = new System.Drawing.Size(105, 63);
            this.Corr2.TabIndex = 3;
            this.Corr2.UseVisualStyleBackColor = true;
            this.Corr2.Click += new System.EventHandler(this.Corr2_Click);
            // 
            // Corr3
            // 
            this.Corr3.Location = new System.Drawing.Point(313, 56);
            this.Corr3.Name = "Corr3";
            this.Corr3.Size = new System.Drawing.Size(105, 63);
            this.Corr3.TabIndex = 4;
            this.Corr3.UseVisualStyleBackColor = true;
            this.Corr3.Click += new System.EventHandler(this.Corr3_Click);
            // 
            // Cont3
            // 
            this.Cont3.Location = new System.Drawing.Point(818, 56);
            this.Cont3.Name = "Cont3";
            this.Cont3.Size = new System.Drawing.Size(105, 63);
            this.Cont3.TabIndex = 7;
            this.Cont3.UseVisualStyleBackColor = true;
            this.Cont3.Click += new System.EventHandler(this.Cont3_Click);
            // 
            // Cont2
            // 
            this.Cont2.Location = new System.Drawing.Point(668, 56);
            this.Cont2.Name = "Cont2";
            this.Cont2.Size = new System.Drawing.Size(105, 63);
            this.Cont2.TabIndex = 6;
            this.Cont2.UseVisualStyleBackColor = true;
            this.Cont2.Click += new System.EventHandler(this.Cont2_Click);
            // 
            // Cont1
            // 
            this.Cont1.Location = new System.Drawing.Point(521, 56);
            this.Cont1.Name = "Cont1";
            this.Cont1.Size = new System.Drawing.Size(105, 63);
            this.Cont1.TabIndex = 5;
            this.Cont1.UseVisualStyleBackColor = true;
            this.Cont1.Click += new System.EventHandler(this.Cont1_Click);
            // 
            // ExitBTN
            // 
            this.ExitBTN.BackColor = System.Drawing.Color.Red;
            this.ExitBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.ExitBTN.ForeColor = System.Drawing.Color.White;
            this.ExitBTN.Location = new System.Drawing.Point(1137, 91);
            this.ExitBTN.Name = "ExitBTN";
            this.ExitBTN.Size = new System.Drawing.Size(112, 57);
            this.ExitBTN.TabIndex = 8;
            this.ExitBTN.Text = "Exit";
            this.ExitBTN.UseVisualStyleBackColor = false;
            this.ExitBTN.Click += new System.EventHandler(this.ExitBTN_Click);
            // 
            // updateBTN
            // 
            this.updateBTN.Location = new System.Drawing.Point(985, 56);
            this.updateBTN.Name = "updateBTN";
            this.updateBTN.Size = new System.Drawing.Size(121, 63);
            this.updateBTN.TabIndex = 9;
            this.updateBTN.Text = "Update button";
            this.updateBTN.UseVisualStyleBackColor = true;
            this.updateBTN.Click += new System.EventHandler(this.updateBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 160);
            this.Controls.Add(this.updateBTN);
            this.Controls.Add(this.ExitBTN);
            this.Controls.Add(this.Cont3);
            this.Controls.Add(this.Cont2);
            this.Controls.Add(this.Cont1);
            this.Controls.Add(this.Corr3);
            this.Controls.Add(this.Corr2);
            this.Controls.Add(this.continuationsLBL);
            this.Controls.Add(this.correctionLBL);
            this.Controls.Add(this.Corr1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Corr1;
        private System.Windows.Forms.Label correctionLBL;
        private System.Windows.Forms.Label continuationsLBL;
        private System.Windows.Forms.Button Corr2;
        private System.Windows.Forms.Button Corr3;
        private System.Windows.Forms.Button Cont3;
        private System.Windows.Forms.Button Cont2;
        private System.Windows.Forms.Button Cont1;
        private System.Windows.Forms.Button ExitBTN;
        private System.Windows.Forms.Button updateBTN;
    }
}

