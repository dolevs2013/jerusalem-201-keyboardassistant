﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace keyboardAssistantGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ExitBTN_Click(object sender, EventArgs e)
        {
            //Exit program safely
            System.Windows.Forms.Application.Exit();
        }

        private void updateBTN_Click(object sender, EventArgs e)
        {
            Response res = Communicator.recvResponse();
            if(res.CorrectionOrContinuation == 1)
            {
                Cont1.Text = res.word1;
                Cont2.Text = res.word2;
                Cont3.Text = res.word3;
            }
            else
            {
                Corr1.Text = res.word1;
                Corr2.Text = res.word2;                                                                                                                                                                                                                                                     
                Corr3.Text = res.word3;
            }
        }

        private void Cont1_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(1, Cont1.Text);
        }

        private void Cont2_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(1, Cont2.Text);
        }

        private void Cont3_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(1, Cont3.Text);
        }

        private void Corr1_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(0, Corr1.Text);
        }

        private void Corr2_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(0, Corr2.Text);
        }

        private void Corr3_Click(object sender, EventArgs e)
        {
            Communicator.SendWord(0, Corr3.Text);
        }
    }
}
