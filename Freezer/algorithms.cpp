#include "algorithms.h"
#include <fstream>
#include <iostream> //remove after check


Algorithms::Algorithms()
{
}

Algorithms::~Algorithms()
{
}

std::vector<std::string> Algorithms::possibleCorrections(std::string word)
{
	std::vector<std::string> corrections = std::vector<std::string>();
	std::string possible_corr, trial;
	char temp;
	bool correct = false;


	// first loop through cashe
	std::string casheRes = Algorithms::checkCashe(word);
	if (casheRes != "not found") {
		corrections.push_back(casheRes);
	}

	std::ifstream file("../words_collection.csv");
	// std::ifstream file("C:/project/jerusalem-201-keyboardassistant/words_collection.csv");
	//loop through the word collection where each word checked is in possible_corr

	if (file.is_open()) {
		std::string line;
		while (std::getline(file, line) && corrections.size() < 5) {
			possible_corr = line.substr(0, line.find_first_of(','));
			if (possible_corr.length() == word.length() + 1)
			{
				//add a letter:
				for (int i = 0; i <= word.length() && !correct; i++)
				{
					for (int c = (int)'a'; c <= (int)'z' && !correct; c++)
					{
						if (i == 0)
						{
							trial = (char)c + word.substr(i, word.length() - i);
						}
						else
						{
							trial = word.substr(0, i) + (char)c + word.substr(i, word.length() - i);
						}

						if (trial == possible_corr)
						{
							corrections.push_back(possible_corr);
							correct = true;
						}
					}
				}
			}
			else if (possible_corr.length() == word.length() - 1)
			{
				//remove a letter:
				for (int i = 0; i <= possible_corr.length() && !correct; i++)
				{
					if (i == 0)
					{
						trial = word.substr(i + 1, word.length() - (i + 1));
					}
					else
					{
						trial = word.substr(0, i) + word.substr(i + 1, word.length() - (i + 1));
					}

					if (trial == possible_corr)
					{
						corrections.push_back(possible_corr);
						correct = true;
					}
				}
			}
			else if (possible_corr.length() == word.length())
			{
				//substitute a letter:
				for (int i = 0; i <= possible_corr.length() && !correct; i++)
				{
					trial = word;
					for (int c = (int)'a'; c <= (int)'z' && !correct; c++)
					{
						trial.replace(i, 1, 1, c);
						if (trial == possible_corr)
						{
							corrections.push_back(possible_corr);
							correct = true;
						}
					}
				}

				//swap two adjacent letters:
				for (int i = 0; i < possible_corr.length() - 1 && !correct; i++)
				{
					trial = word;
					trial[i] = word[i + 1];
					trial[i + 1] = word[i];

					if (trial == possible_corr)
					{
						corrections.push_back(possible_corr);
						correct = true;
					}
				}
			}
			correct = false;
		}
		file.close();
	}

	return corrections;
}


std::string Algorithms::checkCashe(std::string word){
	// Create a text string, which is used to output the text file
	std::string fileText = "";

	// Read from the text file
	std::ifstream casheFile("cashe.txt");

	// Use a while loop together with the getline() function to read the file line by line
	while (getline (casheFile, fileText)) {
		std::string casheKeyWord = "";
		std::string casheFixRes = "";
		char c = 0;
		for (int i = 0; i < fileText.length() - 1; i++) {
			if (fileText[i] == '=') {

				i++;
				while(fileText[i] != ';') {
					casheFixRes += fileText[i];
					i++;
				}
				if (casheKeyWord == word) {
					return casheFixRes;
				}
				else {
					casheKeyWord = "";
					casheFixRes = "";
				}
			}
			else {
				casheKeyWord += fileText[i];
			}
		}
	}
	return "not found";
}

void Algorithms::addToCashe(std::string wrongWord, std::string correction)
{
	bool over50Words = false;
	// first check first dict of words incase of over 50 words. if it is change the var over50Words

	std::string fileText = "";

	std::ifstream casheFile("cashe.txt");

	// Use a while loop together with the getline() function to read the file line by line
	while (getline(casheFile, fileText)) {
		
		// Count variable
		int counter = 0;

		for (int i = 0; i < fileText.length(); i++)

			// checking character in string
			if (fileText[i] == ';') {
				counter++;
			}

		if (counter > 49) {
			over50Words = true;
		}
	}


	// then write to the cashe, if over50Words = true, delete first combination first.


	std::string newCombination = wrongWord;
	newCombination += '=';
	newCombination += correction;
	newCombination += ';';
	// write to string new combination
	std::ofstream myfile;
	myfile.open("cashe.txt");

	// if over50 is true, delete the first one.

	if (over50Words) {
		std::string newString = "";

		int i = 0;
		bool found = false;
		while (i < fileText.length()) {
			if (!found) {
				if (fileText[i] == ';') {
					found = true;
					i++;
				}
				else {
					i++;
				}
			}
			else {
				//std::cout << newString << std::endl;
				newString += fileText[i];
				i++;
			}
		}
		fileText = newString;
	}



	myfile << fileText;
	myfile << newCombination;
	myfile.close();



}


