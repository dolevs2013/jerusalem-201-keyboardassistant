#include "Communicator.h"
#include <iostream>

std::mutex mapLock;

//creates a new thread for each user and inserts them into the user map (m_client)
SOCKET Communicator::startHandleRequests()
{
	this->res = RequestResult();
	try
	{
		bindAndListen();
		//std::cout << "Waiting for application..." << std::endl;
		SOCKET accecptedSocket = accept(_listeningSock, NULL, NULL);
		
		std::thread clientThread = std::thread(&Communicator::handleNewClient, this, accecptedSocket);
		clientThread.detach();
		
		//std::cout << "connected application to socket " << std::to_string(accecptedSocket) << std::endl;

		return accecptedSocket;
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
		std::cout << "last error - " << std::endl;
		int x = WSAGetLastError();
	}
}

void Communicator::startWSA()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
	wVersionRequested = MAKEWORD(2, 2);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) {
		/* Tell the user that we could not find a usable */
		/* Winsock DLL.*/
		//std::string errorMessage = "WSAStartup failed with error: " + std::to_string(err);
		throw std::exception("WSAStartup failed");
	}
}

//creates the listening socket and starts listening
void Communicator::bindAndListen()
{
	//creating socket
	startWSA();
	_listeningSock = socket(AF_INET, SOCK_STREAM, 0);
	if (_listeningSock == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	//socket information
	_sa.sin_port = htons(PORT_NUM);
	_sa.sin_family = AF_INET;
	_sa.sin_addr.s_addr = INADDR_ANY;

	//bind and listen
	if (bind(_listeningSock, (struct sockaddr*)&_sa, sizeof(_sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	if (listen(_listeningSock, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
}

//gets message from socket and sends it to the current handler, does this until user disconnects
void Communicator::handleNewClient(SOCKET clientSock)
{
	try
	{
		while (true)
		{
			this->res = this->getMessage(clientSock);
		}
	}
	catch(disconnectionError e)
	{
		std::cout << "The application disconnected, socket - " << std::to_string(clientSock) << std::endl;
	}
	catch(std::exception e)
	{
		std::cout << "Unexpected error in socket " << std::to_string(clientSock) << std::endl;
		std::cout << "what - " << e.what() << std::endl;
	}
}


//gets a message from client, size is determined in message
RequestResult Communicator::getMessage(SOCKET clientSock)
{
	char* codeBuffer = getData(clientSock, 1);

	//gets data length (always 4 bytes)
	char* lengthBuffer = getData(clientSock, LENGTH_BYTES);
	unsigned int length = 0;
	memcpy(&length, lengthBuffer, LENGTH_BYTES);

	char* dataBuffer = getData(clientSock, length);
	unsigned char* buffer = nullptr;

	//copies code, length, data into a single buffer and terminates it with NULL
	buffer = new unsigned char[length + 1]; //data + NULL
	memcpy(buffer, dataBuffer, length);
	buffer[length] = NULL;

	RequestResult res = deserializeRequest(buffer);

	delete codeBuffer;
	delete lengthBuffer;
	delete[] dataBuffer;

	return res;
}


void Communicator::sendWords(SOCKET sc, int CorrectionOrContinuation, std::vector<std::string> words)
{
	if (words.size() > 0) {
		Words wordsToSend;
		wordsToSend.CorrectionOrContinuation = CorrectionOrContinuation;
		wordsToSend.word1 = words[0];
		wordsToSend.word2 = words[1];
		wordsToSend.word3 = words[2];
		int* len = new int[1];
		unsigned char* buff = Communicator::serializeWordsPacket(wordsToSend, len);
		Communicator::sendData(sc, (char*)buff, *len);
	}
}

//serializes a Words into a buffer
unsigned char* Communicator::serializeWordsPacket(Words response, int* len)
{
	json j;
	j["CorrectionOrContinuation"] = response.CorrectionOrContinuation;
	j["word1"] = response.word1;
	j["word2"] = response.word2;
	j["word3"] = response.word3;
	return serializeJson(j, len, 0);
}

unsigned char* Communicator::serializeJson(json j, int* len, int code)
{
	unsigned int jsonLen = j.dump().length();

	if (len != NULL)
	{
		*len = CODE_LENGTH + LENGTH_BYTES + jsonLen;
	}
	unsigned char* buffer = new unsigned char[CODE_LENGTH + LENGTH_BYTES + jsonLen];
	buffer[0] = code;
	memcpy(buffer + 1, &jsonLen, sizeof(jsonLen));
	std::string jsonStr = j.dump();
	for (int i = 0; i < jsonLen; i++)
	{
		buffer[i + 5] = jsonStr[i];
	}
	return buffer;
}

RequestResult Communicator::deserializeRequest(unsigned char* buffer)
{
	//buffer should only be the jason part for this to work
	json j;
	try
	{
		j = json::parse(buffer);
	}
	catch (std::exception e)
	{
		throw std::exception("Invalid jason in message");
	}
	RequestResult request;
	try
	{
		request.CorrectionOrContinuation = j["CorrectionOrContinuation"];
		request.word = j["word"];
	}
	catch (std::exception e)
	{
		throw std::exception("Invalid jason fields, required fields are: username, password and email");
	}
	return request;
}


//sends a string to socket
void Communicator::sendData(SOCKET sc, std::string message)
{
	sendData(sc, (char*)message.c_str(), message.length());
}

//gets a buffer (array of chars) and sends it to the socket
void Communicator::sendData(SOCKET sc, char* message, int len)
{
	if (send(sc, message, len, 0) == DISCONNECTION)
	{
		throw disconnectionError();
	}
}

//gets bytesNum of data from the socket and returns it, return array should be deleted
char* Communicator::getData(SOCKET sc, int bytesNum)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, NO_FLAGS);

	if (res == DISCONNECTION || res == INVALID_SOCKET)
	{
		delete[] data;
		throw disconnectionError();
	}

	data[bytesNum] = (char)0;

	return data;
}

