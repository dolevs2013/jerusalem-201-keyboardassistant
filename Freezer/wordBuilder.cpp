#include "wordBuilder.h"

WordBuilder::WordBuilder() : key_chars{'.', ',', ' ', '\n', '\r'}
{
	this->cur_word = "";
	this->last_word = "";
}

WordBuilder::~WordBuilder()
{
}

/*
* 
* input:
* output:
*/
bool WordBuilder::getInput(char c)
{
	for (int i = 0; i < sizeof((char*)this->key_chars); i++)
	{
		if (c == this->key_chars[i]) {
			this->last_word = this->cur_word;
			this->cur_word = "";
			return true;
		}


	}
	if (c == '\b') {
		this->cur_word.pop_back();
	}
	else {
		this->cur_word += c;

	}

	return false;
}

std::string WordBuilder::getCurWord()
{
	return this->cur_word;
}

std::string WordBuilder::getLastWord()
{
	return this->last_word;
}

int WordBuilder::getLastWordLen()
{
	return this->last_word_len;
}

void WordBuilder::resetLastWord()
{
	this->last_word_len = this->last_word.length();
	this->last_word = "";
}

void WordBuilder::resetLastWordLen()
{
	this->last_word_len = 0;
}

int WordBuilder::getKeyCharsLen()
{
	int Size = 0;
	while (this->key_chars[Size] != '\0') Size++;
	return Size;
}

const char* WordBuilder::getKeyChars()
{
	return this->key_chars;
}
