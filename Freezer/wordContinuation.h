#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>

#define PATH "writtenWordsCache.txt"

class wordContinuation
{
public:
	wordContinuation();
	~wordContinuation();

	static std::vector<std::string> possibleContinuation(std::string word);

	static std::map<std::string, int> readCache();
	static void addToCache(std::string word);

private:

};

inline::wordContinuation::wordContinuation()
{
}

inline::wordContinuation::~wordContinuation()
{
}
