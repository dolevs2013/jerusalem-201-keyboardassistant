#pragma once
#include <iostream>
#include <string>
#include <algorithm>
#include <array>

class WordBuilder
{
public:
	WordBuilder();
	~WordBuilder();

	bool getInput(char c);
	std::string getCurWord();
	std::string getLastWord();
	int getLastWordLen();
	void resetLastWord();
	void resetLastWordLen();

	int getKeyCharsLen();
	const char* getKeyChars();

private:
	std::string cur_word;
	std::string last_word;
	int last_word_len;
	const char key_chars[];
};
