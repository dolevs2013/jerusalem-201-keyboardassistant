#pragma once
#pragma comment(lib, "ws2_32.lib")
#include <thread>
#include <map>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <vector>

#include "json.hpp"
using nlohmann::json;

using std::map;


//communication defines
#define PORT_NUM 4532
#define NO_FLAGS 0
#define DISCONNECTION 0
#define CODE_LENGTH 1
#define LENGTH_BYTES 4

struct Words
{
	int CorrectionOrContinuation;
	std::string word1;
	std::string word2;
	std::string word3;
};

struct RequestResult
{
	int CorrectionOrContinuation;
	std::string word;
};

class Communicator
{
public:
	SOCKET startHandleRequests();
	static void sendWords(SOCKET sc, int CorrectionOrContinuation, std::vector<std::string> words);

	RequestResult res;
private:
	void startWSA();
	void bindAndListen();
	void handleNewClient(SOCKET clientSock);
	RequestResult getMessage(SOCKET clientSock);
	static unsigned char* serializeWordsPacket(Words response, int* len);
	static unsigned char* serializeJson(json j, int* len, int code);

	static RequestResult deserializeRequest(unsigned char* buffer);

	static void sendData(SOCKET sc, std::string message);
	static void sendData(SOCKET sc, char* message, int len);
	static char* getData(SOCKET sc, int bytesNum);

	SOCKET _listeningSock;
	struct sockaddr_in _sa;

	class disconnectionError : std::exception {};
};