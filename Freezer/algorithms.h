#pragma once
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>

class Algorithms
{
public:
	Algorithms();
	~Algorithms();
	static std::vector<std::string> possibleCorrections(std::string word);

	static std::string checkCashe(std::string word);
	static void addToCashe(std::string wrongWord, std::string correction);

private:

};
