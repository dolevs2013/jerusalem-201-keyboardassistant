﻿// Defines the entry point for the application.

#include "framework.h"
#include "KBfdriver.h"

#include <vector>
#include <Windows.h>
#include <stdio.h>
#include <mutex>

//include algorithms
#include "algorithms.h"
#include "wordBuilder.h"
#include "wordContinuation.h"

#include "Communicator.h"

#define ONE_INTPUT (1)
#define TWO_INTPUT (2)

WordBuilder wordBuild;  // class to build words out of characters pressed.
std::mutex wordBuild_mtx;

Communicator comms;
std::mutex comms_mtx;

SOCKET sc;

// This struct contains the data received by the hook callback. As you see in the callback function
// it contains the thing you will need: vkCode = virtual key code.
KBDLLHOOKSTRUCT kbdStruct;


// Forward declarations of functions included in this code module:
LRESULT CALLBACK    LowLevelKeyboardProc(int, WPARAM, LPARAM);

void mainAlgos(char ch);

void writeWord(const char word[], int wordLen);
void deleteWord(int wordLen);
void movePage();

void handleUserRequests();

INPUT convert_kbdStruct_to_input(KBDLLHOOKSTRUCT kbdStruct)
{
    INPUT input;

    input.type = INPUT_KEYBOARD;
    input.ki.dwExtraInfo = kbdStruct.dwExtraInfo;
    input.ki.dwFlags = kbdStruct.flags;
    input.ki.time = kbdStruct.time;
    input.ki.wScan = (WORD)kbdStruct.scanCode;
    input.ki.wVk = (WORD)kbdStruct.vkCode;

    return input;
}
// Main Function
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    sc = comms.startHandleRequests();
    // Install the low-level keyboard hook    
    HHOOK hhkLowLevelKybd = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, 0, 0); //LowLevelKeyboardProc is the function handaling the keyboard input
    
    std::thread handlerThread = std::thread(handleUserRequests);
    handlerThread.detach();


    // Keep this app running until we're told to stop
    MSG msg;
    if(!GetMessage(&msg, NULL, NULL, NULL)) {    //this while loop keeps the hook

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    
    UnhookWindowsHookEx(hhkLowLevelKybd);

    return 0;
}

LRESULT CALLBACK LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
    kbdStruct = *((KBDLLHOOKSTRUCT*)lParam);

    // if filter driver describes no action (reading),
    if (nCode == HC_ACTION)
    {
        if(wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN)
        {
            mainAlgos(kbdStruct.vkCode);
        }
    }

    return CallNextHookEx(NULL, nCode, wParam, lParam);
}

void mainAlgos(char ch) {
    // if character entered is infact a char, search for possible continuations
    if (ch <= 'Z' && ch >= 'A')
    {
        char c = ch + 32;
        std::vector<std::string> continuationsVec;

        wordBuild_mtx.lock();

        wordBuild.getInput(c);
        continuationsVec = wordContinuation::possibleContinuation(wordBuild.getCurWord());

        wordBuild_mtx.unlock();


        comms_mtx.lock();

        comms.sendWords(sc, 1, continuationsVec);

        comms_mtx.unlock();
    }


    // search if the user has finished writing a word
    wordBuild_mtx.lock();
    const char* keys = wordBuild.getKeyChars();
    for (int i = 0; i < wordBuild.getKeyCharsLen(); i++)
    {
        if (ch == keys[i]) // check if the char entered matches any of the signature ways to finish a word (space, dot etc)
        {
            wordBuild.getInput(ch);
            std::vector<std::string> correctionsVec;
            std::string temp = wordBuild.getLastWord();
            wordBuild_mtx.unlock();
            
            if (temp != "")
            {
                correctionsVec = Algorithms::possibleCorrections(temp);

                comms_mtx.lock();
                comms.sendWords(sc, 0, correctionsVec);
                comms_mtx.unlock();
            }

            wordBuild_mtx.lock();
            wordBuild.resetLastWord();
        }
    }
    wordBuild_mtx.unlock();
}

void writeWord(const char word[], int wordLen) {

    INPUT input[2];
    input[0] = convert_kbdStruct_to_input(kbdStruct);
    input[1] = convert_kbdStruct_to_input(kbdStruct);

    input[0].ki.dwFlags |= LLKHF_INJECTED;


    input[0].ki.dwFlags = KEYEVENTF_UNICODE;
    input[1].ki.dwFlags = KEYEVENTF_KEYUP | KEYEVENTF_UNICODE;

    input[0].ki.wVk = 0;
    input[1].ki.wVk = 0;

    for (int i = 0; i < wordLen; i++) {
        input[0].ki.wScan = (WORD)word[i];
        input[1].ki.wScan = (WORD)word[i];

        // Sending new keyboard input with the new key
        SendInput(TWO_INTPUT, input, sizeof(INPUT));
    }

 }

void deleteWord(int wordLen) {
    // back button need to be activated twice in order to delete

    INPUT input[2];
    input[0] = convert_kbdStruct_to_input(kbdStruct);
    input[1] = convert_kbdStruct_to_input(kbdStruct);

    // Turn the LLKHF_INJECTED flag, so next time I get this action
    // I will know that software generated it
    input[0].ki.dwFlags |= LLKHF_INJECTED;
    input[0].ki.wVk = (WORD)'\b';
    input[1].ki.wVk = (WORD)'\b';
    input[0].ki.dwFlags = 0;
    input[1].ki.dwFlags = KEYEVENTF_KEYUP;



    for (int i = 0; i < wordLen; ++i) {

        // Sending new keyboard input with the new key
        SendInput(TWO_INTPUT, input, sizeof(INPUT));

    }
}

void movePage() {
    // back button need to be activated twice in order to delete

    INPUT input[2];
    input[0] = convert_kbdStruct_to_input(kbdStruct);
    input[1] = convert_kbdStruct_to_input(kbdStruct);
    
    // Turn the LLKHF_INJECTED flag, so next time I get this action
    // I will know that software generated it
    
    input[0].ki.dwFlags |= LLKHF_INJECTED;
    input[0].ki.wVk = VK_MENU;//change to alt

    input[1].ki.dwFlags |= LLKHF_INJECTED;
    input[1].ki.wVk = VK_TAB;
    
    input[0].ki.dwFlags = 0;
    input[1].ki.dwFlags = 0;

    // Sending new keyboard input with the new keys alt + tab
    SendInput(TWO_INTPUT, input, sizeof(INPUT));

    input[0] = convert_kbdStruct_to_input(kbdStruct);
    input[1] = convert_kbdStruct_to_input(kbdStruct);

    // Turn the LLKHF_INJECTED flag, so next time I get this action
    // I will know that software generated it
    input[0].ki.wVk = VK_TAB;
    input[0].ki.dwFlags = KEYEVENTF_KEYUP;

    input[1].ki.wVk = VK_MENU;
    input[1].ki.dwFlags = KEYEVENTF_KEYUP;

    // Sending new keyboard input with the new keys release alt + tab
    SendInput(TWO_INTPUT, input, sizeof(INPUT));
}

void handleUserRequests()
{
    RequestResult result;
    int wordLen = 0;
    while (true)
    {
        Sleep(500);
        comms_mtx.lock();
        
        result = comms.res;
        comms.res = RequestResult();
        
        comms_mtx.unlock();
        
        if (result.word != "")
        {
            movePage();
            Sleep(1000); //z... z.... z...
            if (result.CorrectionOrContinuation == 0)
            {
                // correct the word entered by the user
                wordBuild_mtx.lock();
                
                wordLen = wordBuild.getLastWordLen();
                wordBuild.resetLastWord();
                
                wordBuild_mtx.unlock();
                
                deleteWord(wordLen + 1);

                // write the word
                const char* cStr = result.word.c_str();
                writeWord(cStr, result.word.length());
            }
            else
            {
                // continue the word entered by the user
                wordBuild_mtx.lock();

                wordLen = wordBuild.getCurWord().length();
                wordBuild.getInput(' ');

                wordBuild_mtx.unlock();

                deleteWord(wordLen);

                // write the word
                const char* cStr = result.word.c_str();
                writeWord(cStr, result.word.length());
            }
        }
    }
}
