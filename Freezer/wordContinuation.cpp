#include "wordContinuation.h"

std::vector<std::string> wordContinuation::possibleContinuation(std::string word)
{
	// use read cashe to get file content
	std::map<std::string, int> cacheContent = wordContinuation::readCache();


	// gather all possible continuations
	std::map<std::string, int> possibleWords;

	std::map<std::string, int>::iterator it;

	for (it = cacheContent.begin(); it != cacheContent.end(); it++)
	{
		if (it->first.length() > word.length()) {
			if (word == it->first.substr(0, word.length())) {
				possibleWords.insert(*it);
			}
		}
	}

	// select top 3 by number of appearances  
	std::vector<std::string> sortedPossibleWords;
	sortedPossibleWords.push_back("");
	sortedPossibleWords.push_back("");
	sortedPossibleWords.push_back("");

	for (it = possibleWords.begin(); it != possibleWords.end(); it++)
	{
		if (it->first.length() > word.length()) {
			if (word == it->first.substr(0, word.length())) {
				for (int i = 0; i < 3; i++)
				{
					if (sortedPossibleWords[i] == "")
					{
						sortedPossibleWords[i] = it->first;
						i = 3;
					}
					else if(it->second > possibleWords[sortedPossibleWords[i]])
					{
						int temp = i;
						for (int j = i + 1; j < 3; j++)
						{
							sortedPossibleWords[j] = sortedPossibleWords[i];
							i++;
						}
						sortedPossibleWords[temp] = it->first;
					}
				}
			}
		}
	}
	
	// return the top 3 words.
	return sortedPossibleWords;
}

std::map<std::string, int> wordContinuation::readCache()
{
	std::map<std::string, int> data = std::map<std::string, int>();
	std::ifstream myfile(PATH);
	std::string lineContent = "";

	while (getline(myfile, lineContent)) {
		data.insert(std::pair<std::string, int>(lineContent.substr(0, lineContent.find(':')), std::atoi(lineContent.substr(lineContent.find(':') + 1).c_str())));
	}
	myfile.close();

	return data;
}

void wordContinuation::addToCache(std::string word)
{
	std::ifstream myfile(PATH);
	std::string totalFile = "", fileText = "", lineContent;
	std::string changedNumOfAppearsString;
	bool found = false;

	while (getline(myfile, lineContent)) {
		if (lineContent.find(word) != std::string::npos)
		{
			found = true;

			// change the line content to be increased by one

			std::string numOfAppears = "";

			// first get to the number, go through string until find :
			for (int i = 0; i < lineContent.length(); i++) {
				if (lineContent[i] != ':') {
					changedNumOfAppearsString += lineContent[i];
				}
				else {
					// add +1 to skip the :, and go through the val until 
					changedNumOfAppearsString += lineContent[i];
					i++;
					while (isdigit(lineContent[i])) {
						numOfAppears += lineContent[i];
						i++;
					}
					numOfAppears = std::to_string(atoi(numOfAppears.c_str()) + 1);
					changedNumOfAppearsString += numOfAppears;
					fileText = changedNumOfAppearsString + ";\n";
					totalFile += fileText;
				}
			}
		}
		else
		{
			fileText = lineContent + '\n';
			totalFile += fileText;

		}
	}

	//if (found)
	//{
	//	//!!save last word, save current usages and than add 1, compare new usages and switch accordingly


	//	//continue adding rest of file
	//	while (getline(myfile, lineContent)) {
	//		fileText += lineContent + '\n';
	//	}

	//	totalFile = fileText;
	//}
	//else
	if(!found)
	{
		totalFile = word + ":1;\n" + totalFile;
	}

	myfile.close();

	std::ofstream writefile(PATH);
	writefile << totalFile;
	writefile.close();
}
